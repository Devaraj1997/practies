import React,{Component} from 'react';
import {ImageBackground, StyleSheet, Text, View, TextInput, Button, Linking, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

const image = { uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuIlFi_tVMRuaofI7Eo-CPL9ujbmRw15-o9A&usqp=CAU" };


const App = () => (
  <View style={styles.container}>
       
   <ImageBackground source={image} style={styles.image}>
       <Image source={{uri:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABF1BMVEX/////ZgAAAAAaGhrwYAD5ZAD0YQHlWwDqXgDhWgDJUAHDTgDbWAAXFxe8vLziWwF/f3//WwCtra3SVADQ0NAODg5fX1//YwDWVgD/WgDLy8vDw8P+9vHw8PAMDAz+/PntVwD67OXWTQDAQABERESZmZlMTEyKior+wKT+xaz+zLb949fkVADdTAD0WwDvUwC6PwA1NTVUVFTh4eFtbW2QkJDr6+v+2cn/tpb+r4v+mWz+ejH+bxz+h03+o3r+kFz+dCf5pX/+ejrziV70mHDte0ToZRrvpofxcTLxgEnyv6r50b7plnLqi2Prqo3kcDHikGzjfE7TQQDYXxvswKzWe0/fnoLYkG7gqpXck3Lv2c7pyL4pKSlpOiIkAAAIIUlEQVR4nO2ai1cSWxSHjyAiCsQoCNjIMMNDlBnIfJCmlre6dqXMV2Xa//933L3neeZBl4p7h8Pd31q1hkdr7W/tM2f/zhBjBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEEQBEH8Oupg68XhYWcgx13Iv8PwaE5yeNlR4i5n4mwdS+U5l7I0tx13RZNFPqlyfqZj9TTuoiaJfBzwQ2ZK8WWEICjOzkJ9IUUJzpVTcRc2KZTIDgLSrDRxK7qF0MRXcZc2IV6N6uGcFHdpv4OyfdhRrctRLYS9xv5Gd7sbX6W/yHYKo0sHL+XqaMMBfkE9qUrV18N4C/5ZTq0Bb86D7mhDCQ0Vc1iWq2dxF/0zHDkLU5J/bDjkviwdxV32+Jy6d550+o899D6WhAk5HU6pLP/IsAobzJG301Y7cZc+Ht1eiuvS1g93GpnJ/CiRxNhSX6cQp4evfjQtYB5u85+WX8dd/Dic9VKcYvkPUB418csv4Z71fSgJsKHK/YWFlOdYfq2MzjTlo1DgqU7/A46t3sKC64iSMjsctUylQ8ZO/IZw3047Z6ahpyiprDPScCu0hMvTPzGO5hdsLEUYCMORhgMmp/xvlad/7L+ZBzhFiC3dkYYqU4OPb6b/QPWm7imiY28rZOHphOOAAIZvTUNvqfZeMOaNRz/HjA0C/RVglZ5bhq5k78zOABEt/CN8/sftdcrZ7s/P+xxh2Z30IhWxX8GHVAI8uun2F+d9jpBb3vpijq9fp4F7tCpAMu0vLvoc56FRPT7meIad0MCfq8Zd/hi80xd9jguymXNSYUc8/wYH/knc5Y/BpWnoKdZVNuwHkpy3IoML90Xc5Y9B1zJ0Jftdpvb5lONKRgx8MQ6If6bTvGJ/yNS6P8nZiik5GHfKx3EXPxbnepp3rHeYsuiLObYing4DA1+AaYh00TDtLtX6OWPv+Zzj9BF3lcCxQ4xFytindJpzrL9h7K/6fNgRB/6h7z7EkCMEF8YT3vG9G1bnfSer3qHvQducEMdfCyWTecI51vmwyvURTh3Bn06n/xGGzbnOO/YVdsEZuoo9GPgSHwFEeAxlIxuZjOeowym/7o85JjAo5SofAZxfokTgg57xHPUBxnF/WkXJPp5/ufEowNHQQ8UmPkFFcNQvmKzzKcdRVNiwx0UASaAWQhOtdWo66mcw8n0xx1KEPbbDRXIBnrLxqFom4zjqH80RuRh0hDl5yj1cLQvVQjhhGNmsrZj5xNhHPR10rL+FJNDznliJEdg8lFswzFqN1HHrSacDjnU4KC16Dx6PhfuvfENoYtbqo6GyC90Xc9CxPmRK350dfcF+xkc+alnb0RiAry/mIDAOu65hb/ofk4aRbcNsxrhgXSOTCTjCOHSfy+FsFJA7R9H4wJhh7jk+Ry6u9qf/GWIkV1rFUryGgz8XAUzF9DvnFwBrWxUS5VOlYjreKuxjxsZNq9DY9/ZTgPfC7aMOqmYpwmZ6bmT9jnrHyXL4NE5YBjcVdNQG7M6wR4e1Vs08PrAM+4O4y/wd7kxF7Z51NXc8WluOrrKOjltO/yLuIn+Pe1yolc+MudPR9IM/dpTTL+Mu8Xe5v1mtVG4Zu85meUeM43+CoCG8IC7U1YqmsktndNhJ7oKpkOQMwZeoxcOqpl2wB3tfdZIcZFVDzwjyfPSfUK5uYObfViqeI56oro0PQma1SB6uVXvPsR1xkV7PSANtFDgxeooapBxhc0wYR0W+hBsSp6OmncuBz4Sme/P5wbkeXt3erF65h93BO0PgwObypbHUuL5zmqXIbkvv3hlZ4z6usibI5yWgsfrtkV+RyvCqgilH+xBbXZPDNERJ7fPlY1dmcvfx8lrTrPGoXcVd3gRwDJeWYKO5QTRtddXaVisz0cOvSxyrLrbhDMRSdt9YinZESe0u7vImwGNjebSiJvTp10ZtLPsVecmbmUimy8gIxU9xFzcRvi4vhyUtQe1b3MVNhLvG8ihH7THu4iaCEm2IjlrctU2Ir8+Wl6Mlv8Rd2oRQOUOfY2MWThYmX6IVG7NwsLD5+uzZs0aI2dhIbWRVfQAeLfBSVWdi2BMEQRC/Sr7dLDjX7XZ7pxj6xn6z2YLvNb3vCcVuopRYc16USqVE2LAAX1lnK/B36z8tbULkE8mSa5jMJSMNk4kVMEwmhGzi/9awuLJfaK1b7wYMN/KtwmYrvxFDsb9EpOFuO2Hy/Sm+6zdcKVmfJTbjKvkniTLcTdSSyVwJ/iTyLGC4ksglwbEEn+3FWPZPAIa59vpTk3XbcAfr33luahz4DTcStVqivVloJpLJxG7cxY8FGIKHTS6JhiidA9GDHRDe9BsW1/b29vHfbcLLfNzFjwUa8oDhXg2NgCKo7ozYSws1kQwDPWxC66ydEq8OQoYbxfymuUqFMSy1i+smRes+/J7LJa0PsZvFgGHe3GdrJaEM/XupAnfhd+s1Gq77DdcSJWx5e68kriH2sGb9yB3uIe5CtUL+AC/ENWyW8O5DdkL3odVVxsyXwhpaRgwnf2gvXavlkuYuZLVXBCIM10Gs5MzDfRbqIYqt4GoVI5pGpbZmDQZIe62GmWbDb9jCNFCwhoUgJw0wrAUND3IgUCphAMV7zp/aUNv8qynIfwLLw9b/3HmBkw6dNvasBGA90ijAFRomMGvv7tjpQJAOYkApFt0EDddF6+baeNpqrdjv78K7B+zA+WKxtd9aKYpxDxIEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRBE7PwNteX4mX93nqYAAAAASUVORK5CYII='}}
        style={{width:'40%', height:'30%'}} style={styles.ImageStyle} />  
   </ImageBackground>

   <View style={styles.container2}>
    <Text style={styles.welcome}>Sign In</Text>

    <Icon style={styles.dev} name={'user'} size={20} color='black'></Icon>

      <TextInput style={{borderBottomWidth:1, marginLeft:0, marginRight:0,marginTop:-35,textAlign:'center'}} placeholder="Email"></TextInput>

      <Icon style={styles.deva} name={'lock'} color='black' size={20}></Icon>
      <Icon style={styles.deva1} name={'eye'} size={20} color='black'></Icon> 
      <TextInput style={{borderBottomWidth:1, marginLeft:0, marginRight:0, marginTop:-45, textAlign:'center'}} placeholder="Password" secureTextEntry={true}> </TextInput>

      <Text style={{color:'white', textAlign:'center',backgroundColor:'red',lineHeight:40,borderRadius:20,width:"90%",marginLeft:10,marginTop:20}}>LOGIN</Text>
      <Text style={{color:'red', marginTop:10, marginLeft:20, borderBottomWidth:1,marginRight:225}} onpress={()=>Linking.openURL('http://google.com')}>Forget Password?</Text>
      <Text style={{color:'red',textAlign:"right", marginRight:20, borderBottomWidth:1, marginLeft:290,marginTop:-20}} onpress={()=>Linking.openURL('http://google.com')}>Sign up</Text>

   </View>
   </View> 
);

const styles = StyleSheet.create({
 container: {
   flex: 1,
   flexDirection: "column"
 },

 container2: {
   borderTopLeftRadius:50,
   borderTopRightRadius:60, 
   color:'violet',
   backgroundColor:'white',
   flex:1,
   marginTop:-60
 },

 image: {
   flex: 1,
   resizeMode: "cover",
   justifyContent: "center"
 },

 text: {
   color: "white",
   fontSize: 42,
   fontWeight: "bold",
   textAlign: "center",
   backgroundColor: "#000000a0"
 },

 welcome: {
   fontSize: 28,
   textAlign: 'left',
   marginLeft:20,
   marginTop:40,
   marginBottom:40,
   color: 'black', 
   fontWeight:'bold'
   },

  ImageStyle:{
   marginBottom :-65,
   marginTop:1,
   margin: 130,
   height: 100,
   width: 100,
   resizeMode : 'stretch',
   alignItems: 'center'
  }, 

   ImageStyle2: {
     marginBottom :-45,
     marginTop:1,
     margin: 10,
     height: 25,
     width: 20,
     resizeMode : 'stretch',
     alignItems: 'center'
     },
dev: {
     marginLeft:20,
},

     deva1:{
          marginTop:-10,
          marginLeft:320,
          
     },
  
     deva: {
       marginLeft:20,
       marginTop:10
       
     },
});

export default App;


